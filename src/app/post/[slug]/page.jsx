'use client'
import React, { useState } from 'react';
import { useQuery, gql } from '@apollo/client';
import ReactMarkdown from 'react-markdown';
import {Card, CardHeader, CardBody, Image, Skeleton} from "@nextui-org/react";
import remarkGfm from 'remark-gfm';
import {Loading} from "@/components/atoms/loading"

export default function Post({ params }) {
    const slug = params.slug;
    const QUERY = gql`
        query GET_POST($slug: String!) {
            post(where: { slug: $slug }) {
                content {
                    text
                    markdown
                }
                coverImage {
                    url
                }
                date 
                slug
                title
            }
        }
    `;

    const { data, error,loading } = useQuery(QUERY, { variables: { slug } });

    if (loading) {
        return (
            <Loading/>

        );

    }

    if (error) {
        return (
            <div>
                <p>error</p>
            </div>
        );
    }

    const markdownData = data?.post?.content?.markdown || '';

    return (
        <div>
            <div className="mx-auto w-11/12 md:w-2/3 container   p-4">
                <h1 className="p-6 font-bold text-4xl md:text-6xl">{data?.post?.title}</h1>
                <div>
                    <Image
                        className="rounded-lg shadow-md my-3"
                        src={data?.post?.coverImage?.url}
                        alt={data?.post?.title}
                        width={1920}
                        height={1080}
                    />
                </div>
                <div className=" text-xl w-full">
                    <ReactMarkdown
                        remarkPlugins={[remarkGfm]}
                        components={{
                            h1: (props) => <h1 className="text-3xl font-bold mb-4" {...props} />,
                            h2: (props) => <h2 className="text-2xl font-bold mb-4" {...props} />,
                            h3: (props) => <h3 className="text-xl font-bold mb-4" {...props} />,
                            h4: (props) => <h4 className="text-lg font-bold mb-4" {...props} />,
                            ul: (props) => <ul className="list-disc ml-4 mb-4" {...props} />,
                            ol: (props) => <ol className="list-decimal ml-4 mb-4" {...props} />,
                            p: (props) => <p className="mb-4" {...props} />,
                            code: (props) => <code className="bg-gray-200 px-2 py-1 rounded" {...props} />,
                            em: (props) => <em className="italic" {...props} />,
                            u: (props) => <u className="underline" {...props} />,
                            table: (props) => <table className="w-full border-collapse" {...props} />,
                            th: (props) => <th className="px-4 py-2 border border-gray-400 bg-gray-100" {...props} />,
                            td: (props) => <td className="px-4 py-2 border border-gray-400" {...props} />,
                            blockquote: (props) => <blockquote className="border-l-4 border-gray-400 pl-4 mb-4" {...props} />,
                        }}
                    >
                        {markdownData}
                    </ReactMarkdown>
                </div>
            </div>
        </div>
    );
}
