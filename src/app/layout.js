import "./globals.css";
import {Providers} from "./providers";
import {Footer} from "@/components/organisms/footer";
import Nav from "@/components/organisms/navbar";
import {ApolloWrapper} from "@/lib/apollo-wrapper";


export const metadata = {
  title: "Muzaffer Tas.",
  description: "Ben Muzaffer TAŞ Ankara üniversitesi spor bilimleri fakültesi antrenörlük eğitimi bölümü mezunuyum. Bu sayfamda sizlere spor ve sağlık alanında yararlı olabilecek bilgiler paylaşıp sizleri bilgilendireceğim.",
};




export default function RootLayout({ children }) {
  return (
      <html lang="en" className="dark">
        <body>
        <ApolloWrapper>
        <Nav/>
          <Providers>
           {children}
          </Providers>
        <Footer/>
        </ApolloWrapper>
        </body>

      </html>
  );
}
