'use client'
import {gql, useQuery} from "@apollo/client";
import {Image} from "@nextui-org/react";
import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";
import React from "react";
import {Skeleton} from "@nextui-org/react";

const query = gql`
    query Hakkimda {
        pages(where: {id: "cke1ffer402co0156d87fjc5d"}) {
            id
            content {
                markdown
            }
            title
        }
    }


`

export default function Hakkimda () {

   const {data, error, loading}  = useQuery(query);
   const Page = data?.pages[0]
    if (loading) {
        return (
            <div className="mx-auto w-11/12 md:w-2/3 container p-4">
                <Skeleton height={48} className="p-6 font-bold text-4xl md:text-6xl" />
                <div className="text-xl w-full">
                    <Skeleton height={200} className="mb-4" />
                    {/* Placeholder elements for Markdown rendering */}
                    <Skeleton height={32} className="text-3xl font-bold mb-4" />
                </div>
            </div>
        );

    }


   return (
       <div className="mx-auto w-11/12 md:w-2/3 container p-4">
           <h1 className="p-6 font-bold text-4xl md:text-6xl">{Page?.title}</h1>
           <div className="text-xl w-full">
               <ReactMarkdown
                   remarkPlugins={[remarkGfm]}
                   components={{
                       h1: (props) => <h1 className="text-3xl font-bold mb-4" {...props} />,
                       h2: (props) => <h2 className="text-2xl font-bold mb-4" {...props} />,
                       h3: (props) => <h3 className="text-xl font-bold mb-4" {...props} />,
                       h4: (props) => <h4 className="text-lg font-bold mb-4" {...props} />,
                       ul: (props) => <ul className="list-disc ml-4 mb-4" {...props} />,
                       ol: (props) => <ol className="list-decimal ml-4 mb-4" {...props} />,
                       p: (props) => <p className="mb-4" {...props} />,
                       code: (props) => <code className="bg-gray-200 px-2 py-1 rounded" {...props} />,
                       em: (props) => <em className="italic" {...props} />,
                       u: (props) => <u className="underline" {...props} />,
                       table: (props) => <table className="w-full border-collapse" {...props} />,
                       th: (props) => <th className="px-4 py-2 border border-gray-400 bg-gray-100" {...props} />,
                       td: (props) => <td className="px-4 py-2 border border-gray-400" {...props} />,
                       blockquote: (props) => <blockquote
                           className="border-l-4 border-gray-400 pl-4 mb-4" {...props} />,
                   }}
               >
                   {Page?.content?.markdown}
               </ReactMarkdown>
           </div>
       </div>
   )
}
