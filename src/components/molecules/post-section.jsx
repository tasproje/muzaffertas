'use client'
import React from "react";
import {Card, CardHeader, CardBody, Image, Skeleton} from "@nextui-org/react";
import {gql, useQuery} from "@apollo/client";
import Link from "next/link";
import {Loading} from "@/components/atoms/loading";

const query = gql`
    query Post {
        posts {
            coverImage {
                fileName
                url
            }
            slug
            id
            title
            excerpt
            date 
        }
    }`;

export default function PostSection () {
    const {loading, error, data} = useQuery(query);
    if (loading) {
        return (
            <Loading/>


        );
    }
    return (
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4 m-2 md:m-10">
            {data?.posts?.map((post) => (
                <Link key={post.id} href={`/post/${post.slug}`}>
                    <Card className="py-4 w-full hover:bg-default-100">
                        <CardHeader className="pb-0 pt-2 px-4 flex-col items-start">
                            <h2 className="font-bold  text-2xl md:text-4xl">{post.title}</h2>
                            <small className="text-default-500">
                                {new Date(post.date).toLocaleDateString('tr-TR', {
                                    year: 'numeric',
                                    month: 'long',
                                    day: 'numeric'
                                })}
                            </small>
                        </CardHeader>
                        <CardBody className="overflow-visible py-2">
                            <Image
                                alt="Card background"
                                className="object-unset rounded-xl  h-64"
                                src={post?.coverImage?.url || '/'}
                                width={1920}
                            />
                        </CardBody>
                    </Card>

                </Link>
            ))}
        </div>
    );
}
