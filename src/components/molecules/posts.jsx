'use client'
import {gql, useQuery} from "@apollo/client";
import Link from "next/link";


export default function Posts() {
    const query = gql`
        query Posts {
            posts {
                coverImage {
                    fileName
                    url
                }
                slug
                id
                title
                excerpt
                date
            }
        }`;

    const {loading, error, data} = useQuery(query);


    return (
        <section className="">
            <div className="container px-6 py-10 mx-auto">
                {data?.posts?.map((post) => (
                    <div key={post.id} className="mt-8 lg:-mx-6 lg:flex lg:items-center">
                        <img
                            className="object-cover w-full lg:mx-6 lg:w-1/2 rounded-xl h-72 lg:h-96"
                            src={post.coverImage.url}
                            alt=""
                        />

                        <div className="mt-6 lg:w-1/2 lg:mt-0 lg:mx-6 ">

                            <a
                                href="#"
                                className="block mt-4 text-2xl font-semibold  hover:underline  md:text-3xl"
                            >
                                {post.title}
                            </a>

                            <p className="mt-3 text-sm text-gray-500  md:text-sm">
                                {post.excerpt}
                            </p>

                            <Link href={`post/${post.slug}`} className="inline-block mt-2 text-blue-500 underline hover:text-blue-400">
                                Read more
                            </Link>


                        </div>
                    </div>
                ))}
            </div>
        </section>
    )
}
