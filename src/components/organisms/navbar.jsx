"use client"
import React from "react";
import {useState} from "react"
import {
    Navbar,
    NavbarBrand,
    NavbarContent,
    NavbarItem,
    Button,
    NavbarMenuToggle,
    NavbarMenu, NavbarMenuItem
} from "@nextui-org/react";
import Link from "next/link"
export default function Nav() {
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const menuItems = [
        { label: "Paylaşımlar", href: "/posts" },
        { label: "Hakkımda", href: "/hakkimda" },

    ];

    return (
        <Navbar onMenuOpenChange={setIsMenuOpen}>
            <NavbarContent>
                <NavbarMenuToggle
                    aria-label={isMenuOpen ? "Close menu" : "Open menu"}
                    className="sm:hidden"
                />
                <NavbarBrand>
                    <Link className="font-bold text-white text-inherit text-2xl" href="/">Muzaffer</Link>
                </NavbarBrand>
            </NavbarContent>

            <NavbarContent className="hidden sm:flex gap-4 text-white" justify="center">
                {menuItems.map((item, index) => (
                    <NavbarItem key={`${item.label}-${index}`}>
                        <Link href={item.href}>{item.label}</Link>
                    </NavbarItem>
                ))}

            </NavbarContent>
            <NavbarMenu className="text-white">
                {menuItems.map((item, index) => (
                    <NavbarMenuItem key={`${item.label}-${index}`}>
                        <Link href={item.href}>{item.label}</Link>
                    </NavbarMenuItem>
                ))}

            </NavbarMenu>
        </Navbar>
    );
}
