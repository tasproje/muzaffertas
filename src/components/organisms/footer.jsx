import {Linkedin, Twitter, Instagram} from "lucide-react";
import {Card, CardHeader, CardBody, CardFooter} from "@nextui-org/card";
import Link from "next/link"
export const Footer = () => {
    return (
        <Card className="w-full flex items-center justify-center rounded-b-none ">
            <div className="md:w-2/3 w-full px-4 text-white flex flex-col">
                <CardBody className="flex flex-col">
                    <div className="flex mt-24 mb-12 flex-row justify-between">

                        <Link className="hidden md:block cursor-pointer text-gray-600 hover:text-white uppercase"
                           href="/hakkimda"
                        >Hakkımda</Link>
                        <Link href="/posts" className="hidden md:block cursor-pointer text-gray-600 hover:text-white uppercase">Paylaşımlar</Link>
                        {/*
                        <div className="flex flex-row space-x-8 items-center justify-between">
                            <a>
                                <Twitter/>
                            </a>
                            <a>
                                <Linkedin/>
                            </a>
                            <a>
                                <Instagram/>
                            </a>
                        </div>
                        */}
                    </div>
                    <hr className="border-gray-600"/>
                    <p className="w-full text-center my-12 text-gray-600">Copyright © 2024 Muzaffer Tas</p>
                </CardBody>
            </div>
        </Card>

    )
}
